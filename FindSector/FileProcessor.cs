﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FindSector
{
    public class FileProcessor
    {
        public byte[] ReadFile(string path)
        {
            try
            {
                return File.ReadAllBytes(path);
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine($"Unable to read file from path: {path}, details: {ex.Message}");
                throw new FileNotFoundException($"Details: {ex.Message}");
            }
        }

        public void SaveFileToJson(MatchResult result, string path)
        {
            var json = JsonConvert.SerializeObject(result);
            try
            {
                using (StreamWriter sw = File.CreateText(path))
                {                 
                    sw.Write(json);                  
                }
                Console.WriteLine($"File made at location: {path}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unable to save file in path: {path}, raw data{json}, details: {ex.Message}");               
            }
            
        }

        public MatchResult ProcesssFile(byte[] fileInBytes, int bufferSize, string searchPattern)
        {               
                int blockCount = 1 + (fileInBytes.Length - 1) / bufferSize;
                var matchResult = new MatchResult();
                var options = new ParallelOptions { MaxDegreeOfParallelism = 256 };
                byte[] searchPatternInBytes = Encoding.ASCII.GetBytes(searchPattern);
                int counter = 0;

                Parallel.For(0, blockCount, options, (i) =>
                {
                     ProcessBlock(bufferSize, i, matchResult, searchPatternInBytes,ref counter, fileInBytes);
                });

                Console.WriteLine($"count: {matchResult.Count}");

                return matchResult;
        }

        private void ProcessBlock(int bufferSize, int i, MatchResult matchResult, byte[] searchPatternInBytes, ref int counter, byte[] fileInBytes)
        {
            var offset = i * bufferSize;

            var buffer = new byte[Math.Min(bufferSize, fileInBytes.Length - offset)];

            Interlocked.Increment(ref counter);

            Buffer.BlockCopy(fileInBytes, i * bufferSize, buffer, 0, buffer.Length);

            LocatePatternPositionInBuffer(i, matchResult, searchPatternInBytes, offset, buffer);
        }

        private static void LocatePatternPositionInBuffer(int i, MatchResult jsonObject, byte[] searchPatternInBytes, int offset, byte[] buffer)
        {
            foreach (var position in buffer.Locate(searchPatternInBytes))
            {
                int bufferPosition = offset + position;
                Console.WriteLine($"chunk: {i}, position: {bufferPosition}");

                SaveResultInJsonObject(jsonObject, bufferPosition);
            }
        }

        private static void SaveResultInJsonObject(MatchResult matchResult, int bufferPosition)
        {
            matchResult.Count++;
            matchResult.Positions.Add(bufferPosition);
        }
    }
}
