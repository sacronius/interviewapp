﻿using System.Collections.Generic;

namespace FindSector
{
    public class MatchResult
    {
        public int Count { get; set; }
        public List<int> Positions { get; set; } = new List<int>();
    }
}
