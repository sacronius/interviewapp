﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration. Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace FindSector
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                  .AddJsonFile("appsettings.json", true, true)
                  .Build();
            

            var dataFilePath = configuration.GetSection("DataFilePath").Value;
            var fileToCreate = configuration.GetSection("FileToCreate").Value;
            var patternToSearchJson = configuration.GetSection("PatternToSearch").Value;
            var bufferSize = configuration.GetValue<int>("BufferSize");
            var patternSearch = Regex.Unescape(patternToSearchJson);

            var fileProcessor = new FileProcessor();
            var file = fileProcessor.ReadFile(dataFilePath);
            var matches = fileProcessor.ProcesssFile(file, bufferSize, patternSearch);
            fileProcessor.SaveFileToJson(matches, fileToCreate);
        }

    }
}
