using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace FindSector.Tests
{
    public class FileProcessorTests
    {
        private FileProcessor _fileProcessor;

        public FileProcessorTests()
        {
            _fileProcessor = new FileProcessor();
        }

        [Fact]
        public void ProcesssFile_ShouldReturn_MatchesInFile()
        {
            // Arrange
            var fileName = "./TestFile.txt";
            var expectedBytes = File.ReadAllBytes(fileName);
            var bufferSize = 56;
            var searchPattern = "has";
            // Act
            var result = _fileProcessor.ProcesssFile(expectedBytes, bufferSize,searchPattern);

            // Assert
            Assert.Equal(2, result.Count);
            Assert.Equal(10, result.Positions[0]);
            Assert.Equal(33, result.Positions[1]);
        }

        [Fact]
        public void ProcesssFile_ShouldNotReturn_MatchesInFile()
        {
            // Arrange
            var fileName = "./TestFile.txt";
            var expectedBytes = File.ReadAllBytes(fileName);
            var bufferSize = 56;
            var searchPattern = "";
            
            // Act
            var result = _fileProcessor.ProcesssFile(expectedBytes, bufferSize, searchPattern);

            // Assert
            Assert.Equal(0, result.Count);
        }

        [Fact]
        public void ReadFile_ShouldReturn_FileInBytes()
        {
            // Arrange
            var fileName = "./TestFile.txt";
            var expectedBytes = File.ReadAllBytes(fileName);

            // Act
            var result = _fileProcessor.ReadFile(fileName);

            // Assert
            Assert.Equal(expectedBytes, result);
        }

        [Fact]
        public void ReadFile_ShouldThrow_FileNotFoundException()
        {
            // Arrange
            var fileName = "./TestFile1.txt";

            // Act & Assert
            Assert.Throws<FileNotFoundException>(() => _fileProcessor.ReadFile(fileName));
        }


    }
}
