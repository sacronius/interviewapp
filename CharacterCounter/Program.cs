﻿using System;

namespace CharacterCounter
{
    partial class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Give your sequence of characters");
            var input = Console.ReadLine();

            ArrayStringCounter arrayCounter = new ArrayStringCounter();
            Console.WriteLine("Display by count order in array: ");
            arrayCounter.DisplayByCountOrder(input);
            Console.WriteLine("Display in alphabetically order in array: ");
            arrayCounter.DisplayInAphalphabeticallyOrder(input);

            ListStringCounter listCounter = new ListStringCounter();
            Console.WriteLine("Display by count order in list: ");
            listCounter.DisplayByCountOrder(input);
            Console.WriteLine("Display string alphabetically in list: ");
            listCounter.DisplayInAphalphabeticallyOrder(input);

            Console.ReadLine();
        }
    }
}
