﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CharacterCounter
{
    public class ListStringCounter 
    {
        public List<KeyValuePair<char,int>> DisplayByCountOrder(string stringValue)
        {
            
            var dict = GetStringStatistics(stringValue);
            var query = dict.OrderByDescending(x => x.Value).ThenBy(x => x.Key).ToList();

            foreach (var item in query)
            {
                Console.WriteLine($"key = {item.Key}, value = {item.Value}");
            }
            return query;
        }

        public List<KeyValuePair<char,int>> DisplayInAphalphabeticallyOrder(string stringValue)
        {
            var dict = GetStringStatistics(stringValue);
            var query =dict.OrderBy(x => x.Key).ToList();

            foreach (var item in query)
            {
                Console.WriteLine($"key = {item.Key}, value = {item.Value}");
            }

            return query;
        }

        public Dictionary<char, int> GetStringStatistics(string stringValue)
        {
            var testString = stringValue.ToUpper();
            var distinctChars = testString.Distinct().ToList();
            var dict = new Dictionary<char, int>();
            distinctChars.ForEach(x => dict.Add(x, testString.Count(y => y == x)));

            return dict;
        }
    }
}
