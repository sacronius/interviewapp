﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CharacterCounter
{
    public class ArrayStringCounter
    {
        public KeyValuePair<char, int>[] DisplayByCountOrder(string stringValue)
        {
            var keyValuePair = GetStringStatistics(stringValue);
            var query = keyValuePair.OrderByDescending(x => x.Value).ThenBy(x=>x.Key).ToArray();

            foreach (var item in query)
            {
                Console.WriteLine($"key = {item.Key}, value = {item.Value}");
            }

            return query;
        }

        public KeyValuePair<char,int>[] DisplayInAphalphabeticallyOrder(string stringValue)
        {
            var keyValuePair = GetStringStatistics(stringValue);
            var query = keyValuePair.OrderBy(x => x.Key).ToArray();

            foreach (var item in query)
            {
                Console.WriteLine($"key = {item.Key}, value = {item.Value}");
            }

            return query;
        }

        public KeyValuePair<char,int>[] GetStringStatistics(string stringValue)
        {            
            var testString = stringValue.ToUpper();
            var distinctChars = testString.Distinct().ToArray();
            var keyValuePair = new KeyValuePair<char, int>[distinctChars.Length]; 

            for (int i = 0; i <= distinctChars.Length -1; i++)
            {

                keyValuePair[i] = new KeyValuePair<char, int>(distinctChars[i], testString.Count(f=>f == distinctChars[i]));
            }

            return keyValuePair;
        }
    }
}
