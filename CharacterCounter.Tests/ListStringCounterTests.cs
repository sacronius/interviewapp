﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace CharacterCounter.Tests
{
    public class ListStringCounterTests
    {
        private ListStringCounter _listStringCounter;

        public ListStringCounterTests()
        {
            _listStringCounter = new ListStringCounter();
        }

        [Fact]
        public void DisplayByCountOrder_ShouldReturn_ListKeyValuePairWithSortedCount()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testDictionary = _listStringCounter.GetStringStatistics(stringToTest);
            var expected = testDictionary.OrderByDescending(x => x.Value).ThenBy(x => x.Key).ToList();


            // Act
            var result = _listStringCounter.DisplayByCountOrder(stringToTest);

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void DisplayByCountOrder_ShouldReturn_ListKeyValuePairWithNotSortedCount()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testDictionary = _listStringCounter.GetStringStatistics(stringToTest);
            var expected = testDictionary.OrderBy(x => x.Key).ToList();


            // Act
            var result = _listStringCounter.DisplayByCountOrder(stringToTest);

            // Assert
            Assert.NotEqual(expected, result);
        }

        [Fact]
        public void DisplayInAphalphabeticallyOrder_ShouldReturn_ListKeyValuePairInTheCorrectOrder()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testDictionary = _listStringCounter.GetStringStatistics(stringToTest);
            var expected = testDictionary.OrderBy(x => x.Key).ToList();

            // Act
            var result = _listStringCounter.DisplayInAphalphabeticallyOrder(stringToTest);

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void DisplayInAphalphabeticallyOrder_ShouldReturn_ListKeyValuePairInTheNotCorrectOrder()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testDictionary = _listStringCounter.GetStringStatistics(stringToTest);
            var expected = testDictionary.OrderByDescending(x => x.Key).ToList();

            // Act
            var result = _listStringCounter.DisplayInAphalphabeticallyOrder(stringToTest);

            // Assert
            Assert.NotEqual(expected, result);
        }

        [Fact]
        public void GetStringStatistics_ShouldReturn_DictionaryWithStringStatistics()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testString = stringToTest.ToUpper();
            var dict = new Dictionary<char, int>();
            var disctinct = testString.Distinct().ToList();
            disctinct.ForEach(x => dict.Add(x, testString.Count(y => y == x)));

            // Act
            var result = _listStringCounter.GetStringStatistics(stringToTest);

            // Assert
            Assert.Equal(dict, result);
        }
    }
}
