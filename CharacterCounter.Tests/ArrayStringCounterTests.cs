using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CharacterCounter.Tests
{
    public class ArrayStringCounterTests
    {
        private ArrayStringCounter _arrayStringCounter;

        public ArrayStringCounterTests()
        {
           
            _arrayStringCounter = new ArrayStringCounter();
        }

        [Fact]
        public void DisplayByCountOrder_ShouldReturn_KeyValuePairArrayWithSortedCount()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testDictionary = _arrayStringCounter.GetStringStatistics(stringToTest);
            var expected = testDictionary.OrderByDescending(x => x.Value).ThenBy(x => x.Key).ToArray();

            // Act
            var result = _arrayStringCounter.DisplayByCountOrder(stringToTest);

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void DisplayByCountOrder_ShouldReturn_KeyValuePairArrayWithNotSortedCount()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testDictionary = _arrayStringCounter.GetStringStatistics(stringToTest);
            var expected = testDictionary.OrderBy(x => x.Key).ToArray();

            // Act
            var result = _arrayStringCounter.DisplayByCountOrder(stringToTest);

            // Assert
            Assert.NotEqual(expected, result);
        }

        [Fact]
        public void DisplayInAphalphabeticallyOrder_ShouldReturn_KeyValuePairArrayInTheCorrectOrder()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testDictionary = _arrayStringCounter.GetStringStatistics(stringToTest);
            var expected = testDictionary.OrderBy(x => x.Key).ToArray();

            // Act
            var result = _arrayStringCounter.DisplayInAphalphabeticallyOrder(stringToTest);

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void DisplayInAphalphabeticallyOrder_ShouldReturn_KeyValuePairArrayInTheNotCorrectOrder()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testDictionary = _arrayStringCounter.GetStringStatistics(stringToTest);
            var expected = testDictionary.OrderByDescending(x => x.Key).ToArray();

            // Act
            var result = _arrayStringCounter.DisplayInAphalphabeticallyOrder(stringToTest);

            // Assert
            Assert.NotEqual(expected, result);
        }

        [Fact]
        public void GetStringStatistics_ShouldReturn_KeyValuePairWithStringStatistics()
        {
            // Arrange
            var stringToTest = "MmaAtThH1#";
            var testString = stringToTest.ToUpper();
            var distinctChars = testString.Distinct().ToArray();
            var keyValuePair = new KeyValuePair<char, int>[distinctChars.Length];
            for (int i = 0; i <= distinctChars.Length - 1; i++)
            {

                keyValuePair[i] = new KeyValuePair<char, int>(distinctChars[i], testString.Count(f => f == distinctChars[i]));
            }

            // Act
            var result = _arrayStringCounter.GetStringStatistics(stringToTest);

            // Assert
            Assert.Equal(keyValuePair, result);
        }
    }
}
